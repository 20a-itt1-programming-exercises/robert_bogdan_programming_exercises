Python 3.8.5 (tags/v3.8.5:580fbb0, Jul 20 2020, 15:43:08) [MSC v.1926 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
#EX2
>>> name = input ("Enter your name:")
Enter your name:Robert
>>> print(name)
Robert'
>>> print("Hello",name)
Hello Robert
#EX3
>>> hours = float(input("Enter Hours:"))
Enter Hours:35
>>> rate = float(input("Enter rate:"))
Enter rate:2.75
>>> pay = hours * rate
>>> print (pay)
96.25
#EX4
>>> width = 17
>>> height = 12.0
>>> print (width//2)
8
>>> print (width/2.0)
8.5
>>> print (height/3)
4.0
>>> 1+2*5
11
#EX5
>>> celcius = float (input("Write degrees in Celsius:"))
Write degrees in Celsius:13
>>> fahrenheit = celcius*1.8+32
>>> print(fahrenheit)
55.400000000000006
>>> 