import pygame

pygame.display.set_caption('Hunting for a wild bird')

def inter(x1, y1, x2, y2, db1, db2):
    if x1 > x2 - db1 and x1 < x2 + db2 and y1 > y2 - db1 and y1 < y2+db2:
        return 1
    else:
        return 0

pygame.init()


window = pygame.display.set_mode((500, 500))
screen = pygame.Surface((500, 500))


player = pygame.Surface ((40,40))

zet = pygame.Surface((25,19))

arrow = pygame.Surface((5,20))

count = 0

player.set_colorkey(0, 0,)

arrow.set_colorkey(0, 0,)

zet.set_colorkey(0, 0,)

img_a = pygame.image.load('archer.png')
img_b = pygame.image.load('arrow.png')
img_c = pygame.image.load('zet.png')

myfont = pygame.font.SysFont('monospace', 15)

x_a = 1000
y_a = 1000

strike = False



x_z = 0
y_z= 0

x_p = 0
y_p = 460

right = True

pygame.key.set_repeat(5,5)
done = False
while done == False:
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            done = True
        if e.type == pygame.KEYDOWN and e.key == pygame.K_a:
            x_p -= 1
        if e.type == pygame.KEYDOWN and e.key == pygame.K_d:
            x_p += 1
        if e.type == pygame.KEYDOWN and e.key == pygame.K_SPACE:
            if strike == False:
                strike = True
                x_a = x_p 
                y_a = y_p - 35




    if strike:
           y_a -= 0.13
           if y_a < 0:
               strike = False
               y_a = 1000
               x_a = 1000

               
    if inter(x_a, y_a, x_z, y_z, 10, 20):
        count += 1
        strike = False
        y_a = 1000
        x_a = 1000
        
                

    if right:
        x_z += 0.1
        if x_z > 460:
           x_z -= 0.1
           right = False
    else:
        x_z -= 0.14
        if x_z < 0:
            x_z += 0.14
            right = True
    

    string = myfont.render('Score: '+str(count), 0, (255,0,0))
    
    screen.fill((255,215,0))
    player.blit(img_a,(0,0))
    arrow.blit(img_b,(0,0))
    zet.blit(img_c,(0,0))
    screen.blit(string,(420,420))
    screen.blit(arrow,(x_a,y_a))
    screen.blit(zet, (x_z,y_z))
    screen.blit(player,(x_p,y_p))
    window.blit(screen, (0, 0))      
    pygame.display.update()

pygame.quit()

