#Exercise 1: Write a program which repeatedly reads numbers until the
#user enters “done”. Once “done” is entered, print out the total, count,
#and average of the numbers. If the user enters anything other than a
#number, detect their mistake using try and except and print an error
#message and skip to the next number.

tot=0
avg=0
count=0

while True:
    try:
        x = input("Enter a number:")
        if (x == "done"):
         break
        value = float(x)
        tot = value + tot
        count = count + 1
        avg = tot / count
    except ValueError:
        print("Bad Score")
print(tot, avg, count)
        
    






#Exercise 2: Write another program that prompts for a list of numbers
#as above and at the end prints out both the maximum and minimum of
#the numbers instead of the average.

tot=0
avg=0
count=0
large=None
smallest=None
while True:
    try:
        x = input("Enter a number:")
        if (x == "done"):
         break
        value = float(x)
        if smallest is None or value<smallest:
            smallest = value
        if large is None or value>large:
            large = value
    except ValueError:
        print("Bad Score")
print(large, smallest)
        
    
