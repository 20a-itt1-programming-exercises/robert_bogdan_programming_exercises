#Exercise 1: Revise a previous program as follows: Read and parse the
#“From” lines and pull out the addresses from the line. Count the number of messages from each person using a dictionary.
#After all the data has been read, print the person with the most commits
#by creating a list of (count, email) tuples from the dictionary. Then
#sort the list in reverse order and print out the person who has the most
#commits.


name = input ('Enter the file name:')
handle = open(name)
ddict = dict()
for line in handle:
	words = line.split()
	if(len(words)<2 or words[0]!='From') : continue
	if(words[1] not in ddict):
		ddict[words[1]]=1
	else:
		ddict[words[1]] = ddict[words[1]] + 1

lst = list()
for k,v in list(ddict.items()):
	lst.append((v,k))
lst.sort(reverse=True)
for k,v in lst:
	print(k,v)




#Exercise 2: This program counts the distribution of the hour of the day
#for each of the messages. You can pull the hour from the “From” line
#by finding the time string and then splitting that string into parts using
#the colon character. Once you have accumulated the counts for each
#hour, print out the counts, one per line, sorted by hour as shown below.


name = input ('Enter the file name:')
handle = open(name)
ddict = dict()
for line in handle:
	words = line.split()
	if(len(words)<2 or words[0]!='From') : continue
	time = words[5].split(":")
	time = time[0]
	if(time not in ddict):
            
                ddict[time] = 1
	else:
	        ddict[time] = ddict[time] + 1
lst = list()
for k,v in list(ddict.items()):
	lst.append((k,v))
lst.sort(reverse=False)
for k,v in lst:
	print(k,v)

