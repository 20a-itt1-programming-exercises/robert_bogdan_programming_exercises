#Exercise 1: Write a program that reads the words in words.txt and stores them as
#keys in a dictionary. It doesn’t matter what the values are. Then you
#can use the in operator as a fast way to check whether a string is in the
#dictionary.

name = input ('Enter the file name:')
handle = open(name)

counts = dict()
for line in handle:
    words = line.split()
    for word in words:
         counts[word]= ''
print(counts)



#Exercise 2: Write a program that categorizes each mail message by
#which day of the week the commit was done. To do this look for lines
#that start with “From”, then look for the third word and keep a running
#count of each of the days of the week. At the end of the program print
#out the contents of your dictionary (order does not matter).


name = input ('Enter the file name:')
handle = open(name)
days={'Mon':0,'Tue':0,'Wed':0,'Thu':0,'Fri':0,'Sat':0,'Sun':0}
for line in handle:
    words = line.split()
    if len(words)<3 or words[0]!= 'From' : continue
    days[words[2]] = days[words[2]] + 1

print(days)


#Exercise 3: Write a program to read through a mail log, build a histogram using a dictionary to count how many messages have come from
#each email address, and print the dictionary.



name = input ('Enter the file name:')
handle = open(name)
ddict = dict()
for line in handle:
	words = line.split()
	if(len(words)<2 or words[0]!='From') : continue
	if(words[1] not in ddict):
		ddict[words[1]]=1
	else:
		ddict[words[1]] = ddict[words[1]] + 1
print(ddict)
print(words)



#Exercise 4: Add code to the above program to figure out who has the
#most messages in the file. After all the data has been read and the dictionary has been created, look through the dictionary using a maximum
#loop (see Chapter 5: Maximum and minimum loops) to find who has
#the most messages and print how many messages the person has.


name = input ('Enter the file name:')
handle = open(name)
ddict = dict()
for line in handle:
	words = line.split()
	if(len(words)<2 or words[0]!='From') : continue
	if(words[1] not in ddict):
		ddict[words[1]]=1
	else:
		ddict[words[1]] = ddict[words[1]] + 1
maxcount = None
person = None
for word,count in ddict.items():
    if maxcount is None or count > maxcount:
         maxcount = count
         person = word
print(person,maxcount)




#Exercise 5: This program records the domain name (instead of the
#address) where the message was sent from instead of who the mail came
#from (i.e., the whole email address). At the end of the program, print
#out the contents of your dictionary.

name = input ('Enter the file name:')
handle = open(name)
ddict = dict()
for line in handle:
    words = line.split()
    if(len(words)<2 or words[0]!='From') : continue
    frommail=words[1].split('@')
    frommail=frommail[1]
    if(frommail not in ddict):
	    ddict[frommail]=1
    else:
	    ddict[frommail] = ddict[frommail] + 1
print(ddict)
