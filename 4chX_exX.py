#Exercise 4 : What is the purpose of the “def” keyword in Python?
b) It indicates the start of a function

#Exercise 5 : What will the following Python program print out?
d) ABC Zap ABC


#EX 6
hours = float(input("Enter Hours:"))
rate = float(input("Enter Rate:"))
extra=1.5
def computepay(hours, rate):
    if(hours<=40):
        pay=hours*rate
    if(hours>40):
        pay=40*rate+((hours-40)*rate*extra)
    return pay                                                      
pay = computepay(hours, rate)                                   
print(pay)



#EX 7
def computegrade(score):
	try:
		score = float(score)
		grade="";
		if(score>1.0 or score<0.0):
			grade='Bad Score'
		elif(score>=0.9):
			grade='A'
		elif(score>=0.8):
			grade='B'
		elif(score>=0.7):
			grade='C'
		elif(score>=0.6):
			grade='D'
		elif(score<0.6):
			grade='F'
		return grade
	except:
		print('Bad Score')
		return 'Inncorect'

score = float(input("Enter the score between 0.0 and 1.0"))
grade=computegrade(score)
print(' Grade is ' + grade )
